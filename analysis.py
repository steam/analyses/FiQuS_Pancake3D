from steam_sdk.analyses.AnalysisSTEAM import AnalysisSTEAM

# Initialize STEAM analysis to run FiQuS multiple times with different parameters:
analysis = AnalysisSTEAM(file_name_analysis="analysis.yaml", verbose=True)

# Start running FiQuS multiple times with STEAM analysis:
analysis.run_analysis()