# FiQuS Pancake3D

This repository contains the required FiQuS input file and a Python script to reproduce the results presented in S. Atalay et al., "An open-source 3D FE quench simulation tool for no-insulation HTS pancake coils", in Superconductor Science and Technology, DOI: [10.1088/1361-6668/ad3f83](https://doi.org/10.1088/1361-6668/ad3f83).

For using an updated version of FiQuS (i.e., 2024.12.x), please refer to the [FiQuS Pancake3D 2024.12.x](https://gitlab.cern.ch/steam/analyses/FiQuS_Pancake3D/-/tree/fiqus_2024.12.x) branch.

## Start Guide

1.  Install Python 3.11.8.
2.  Clone the repository by running the code below in a terminal
    ```
    git clone https://gitlab.cern.ch/steam/analyses/FiQuS_Pancake3D.git
    ```
    or download it as a zip from [here](https://gitlab.cern.ch/steam/analyses/FiQuS_Pancake3D/-/archive/master/FiQuS_Pancake3D-master.zip).
3.  Download the CERNGetDP 2024.3.0 release from [here](https://gitlab.cern.ch/steam/cerngetdp/-/releases/2024.3.0).
4.  Update `GetDP_path` in `analysis.yaml` file to point the path to the GetDP executable downloaded in Step 3.
    ```yaml
    GetDP_path: C:\some\path\to\getdp\getdp_2024.3.0.exe
    ```
5.  Install FiQuS 2024.5.0 and steam_sdk 2024.5.0 by running one of the codes below in a terminal
    ```
    pip install fiqus==2024.5.0
    pip install steam-sdk==2024.5.0
    ```
    or
    ```
    pip install -r requirements.txt
    ```
6.  Run analysis.py to reproduce the results found in the paper.
    ```
    python analysis.py
    ```